# salvus_seismo

Interface to easy the use of the Salvus wave propagation solver for
seismologists.

It currently does the following things:

* Perform coordinate transformations between Earth/spherical coordinates to Salvus' internal coordinate system.
* Create seismic (tensor and vectorial) sources and receivers either by hand or directly via [ObsPy](http://obspy.org) - for example via StationXML or QuakeML files or acquiring that information directly from web services.
* Place receivers and sources exactly relative to mesh surface.
* Generate the required command line call for Salvus.
* Generate the `receivers.toml` file Salvus requires.
* Automatically select which boundaries should be absorbing.
* ...


The main design goal of `salvus_seismo` is too make it as easy and reliable as
possible for seismologists to use Salvus while also taking over as many
complexities as possible so that `salvus_wave` can focus on the actual
numerical waveform propagation.


## Usage and Installation Instructions

See http://salvus.io
