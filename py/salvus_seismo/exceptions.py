class SalvusSeismoError(Exception):
    pass


class ReceiverParseError(SalvusSeismoError):
    pass


class SourceParseError(SalvusSeismoError):
    pass
