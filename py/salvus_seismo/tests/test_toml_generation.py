#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
:copyright:
    Lion Krischer (lionkrischer@gmail.com), 2017
:license:
    MIT License
'''
import os

import numpy as np
import pytest
import toml

import obspy
import salvus_seismo

_DATA = os.path.join(os.path.dirname(__file__), "data")


@pytest.mark.parametrize("longitude, expected_x_y, transform_matrix", [
    (0.0, [6371000.0, 0.0], [[1.0, 0.0], [0.0, 1.0]]),
    (90.0, [0.0, 6371000.0], [[0.0, 1.0], [-1.0, 0.0]]),
    (180.0, [-6371000.0, 0.0], [[-1.0, 0.0], [0.0, -1.0]]),
    (270.0, [0.0, -6371000.0], [[0.0, -1.0], [1.0, 0.0]]),
    (360.0, [6371000.0, 0.0], [[1.0, 0.0], [0.0, 1.0]]),
    (-90.0, [0.0, -6371000.0], [[0.0, -1.0], [1.0, 0.0]]),
    (-180.0, [-6371000.0, 0.0], [[-1.0, 0.0], [0.0, -1.0]]),
    (-270.0, [0.0, 6371000.0], [[0.0, 1.0], [-1.0, 0.0]])
])
def test_2d_receiver(tmpdir, longitude, expected_x_y, transform_matrix):
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        start_time=0.0,
        end_time=10.0,
        polynomial_order=4,
        verbose=True,
        dimensions=2)

    # Latitude is 0 for 2D receivers.
    receiver = salvus_seismo.Receiver(
        longitude=longitude,
        network="AB",
        station="CED",
        location="")

    source = salvus_seismo.Source(
        longitude=45.0,
        depth_in_m=12000,
        # All t components can be ignored for 2D receivers.
        m_rr=4.710000e+17,
        m_pp=-4.740000e+17,
        m_rp=-8.050000e+16,
        origin_time=obspy.UTCDateTime(2011, 1, 2, 3, 4, 5),
        sliprate="ricker")

    folder = os.path.join(str(tmpdir), "example")
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[receiver],
        config=c,
        output_folder=folder)

    # Now open the file and check it.
    with open(os.path.join(folder, "receivers.toml"), "rt") as fh:
        recs = toml.loads(fh.read())

    assert recs == {'receiver': [
        {'network': 'AB', 'station': 'CED', 'location': '',
         'physical_latitude': 0.0,
         'physical_longitude': longitude,
         'physical_depth_in_meters': 0.0,
         'medium': 'solid',
         'salvus_coordinates': expected_x_y,
         'transform_matrix': transform_matrix}]}


@pytest.mark.parametrize("lat, lon, expected_x_y, transform_matrix", [
    (0.0,
     0.0,
     [6371000.0, 0.0, 0],
     [[1.0, 0.0, 0.0],
      [0.0, 0.0, 1.0],
      [0.0, 1.0, 0.0]]),
    (0.0,
     90.0,
     [0.0, 6371000.0, 0.0],
     [[0.0, 1.0, 0.0],
      [0.0, 0.0, 1.0],
      [-1.0, 0.0, 0.0]]),
    (0.0,
     180.0,
     [-6371000.0, 0.0, 0.0],
     [[-1.0, 0.0, 0.0],
      [0.0, 0.0, 1.0],
      [0.0, -1.0, 0.0]]),
    (90.0,
     0.0,
     [0.0, 0.0, 6371000.0],
     [[0.0, 0.0, 1.0],
      [-1.0, 0.0, 0.0],
      [0.0, 1.0, 0.0]]),
])
def test_3d_receiver(tmpdir, lat, lon, expected_x_y, transform_matrix):
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        start_time=0.0,
        end_time=10.0,
        polynomial_order=4,
        verbose=True,
        dimensions=3)

    # Latitude is 0 for 2D receivers.
    receiver = salvus_seismo.Receiver(
        latitude=lat,
        longitude=lon,
        network="AB",
        station="CED",
        location="")

    source = salvus_seismo.Source(
        longitude=45.0,
        depth_in_m=12000,
        # All t components can be ignored for 2D receivers.
        m_rr=4.710000e+17,
        m_pp=-4.740000e+17,
        m_rp=-8.050000e+16,
        origin_time=obspy.UTCDateTime(2011, 1, 2, 3, 4, 5),
        sliprate="ricker")

    folder = os.path.join(str(tmpdir), "example")
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[receiver],
        config=c,
        output_folder=folder)

    # Now open the file and check it.
    with open(os.path.join(folder, "receivers.toml"), "rt") as fh:
        recs = toml.loads(fh.read())

    assert recs == {'receiver': [
        {'network': 'AB', 'station': 'CED', 'location': '',
         'physical_latitude': lat,
         'physical_longitude': lon,
         'physical_depth_in_meters': 0.0,
         'medium': 'solid',
         'salvus_coordinates': expected_x_y,
         'transform_matrix': transform_matrix}]}


@pytest.mark.parametrize("lat, lng, depth, x, y", [
    (0.0, 0.0, 0.0, 6371000.0, 0.0)
])
def test_2d_receiver_placement_in_depth(tmpdir, lat, lng, depth, x, y):
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e", start_time=0.0, end_time=10.0,
        polynomial_order=4, verbose=True, dimensions=2)
    receiver = salvus_seismo.Receiver(depth_in_m=depth)
    folder = os.path.join(str(tmpdir), "example")
    salvus_seismo.generate_cli_call(
        source=salvus_seismo.Source(),
        receivers=[receiver],
        config=c, output_folder=folder,
        exodus_file="")

    # Now open the file and check it.
    with open(os.path.join(folder, "receivers.toml"), "rt") as fh:
        recs = toml.loads(fh.read())

    np.testing.assert_allclose(
        recs["receiver"][0]["salvus_coordinates"],
        (x, y))
