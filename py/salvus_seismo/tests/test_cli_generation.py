#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
:copyright:
    Lion Krischer (lionkrischer@gmail.com), 2017
:license:
    MIT License
'''
import os

import numpy as np
import obspy
import toml
import pytest

import salvus_seismo


def __parse_cmdline(line):
    attrs = {}
    key = None
    line = line.split()
    for item in line:
        if item.startswith("--"):
            item = item.lstrip("-")
            key = item
        else:
            if key is None:
                continue
            attrs[key] = item
    return attrs


@pytest.mark.parametrize("lat, lon, f_r, f_t, f_p, s_x, s_y, expected", [
    (0, 0, 1, 2, 3, 6371000.0, 0.0, [1, 3, -2]),
    (0, 180, 1, 2, 3, -6371000.0, 0.0, [-1, -3, -2])
])
def test_3d_force_source(tmpdir, lat, lon, f_r, f_t, f_p, s_x, s_y, expected):
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        start_time=0.0,
        end_time=10.0,
        polynomial_order=4,
        verbose=True,
        dimensions=3,
        with_anisotropy=True)

    source = salvus_seismo.ForceSource(
        latitude=lat,
        longitude=lon,
        depth_in_m=0.0,
        # Up
        f_r=f_r,
        # South
        f_t=f_t,
        # East
        f_p=f_p,
        origin_time=obspy.UTCDateTime(2011, 1, 2, 3, 4, 5),
        sliprate="ricker",
        center_frequency=0.0005)

    folder = os.path.join(str(tmpdir), "example")
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)

    # Now open the file and check it.
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()

    assert cli.startswith("salvus")

    attrs = __parse_cmdline(cli)

    assert int(attrs["dimension"]) == 3
    assert attrs["mesh-file"] == "example_mesh.e"
    assert attrs["model-file"] == "example_mesh.e"
    assert float(attrs["start-time"]) == 0.0
    assert float(attrs["end-time"]) == 10.0
    assert int(attrs["polynomial-order"]) == 4

    assert float(attrs["start-time"]) == 0.0
    assert float(attrs["end-time"]) == 10.0

    assert attrs["receiver-toml"] == os.path.join(folder, "receivers.toml")
    assert attrs["source-toml"] == os.path.join(folder, "source.toml")
    assert attrs["receiver-fields"] == "u_ELASTIC"
    assert attrs["receiver-file-name"] == "receiver.h5"

    # Test source toml.
    with open(os.path.join(folder, "source.toml"), "rt") as fh:
        src = toml.loads(fh.read())

    src_location = (src['source'][0]['location'])
    np.testing.assert_allclose(src_location, [s_x, s_y, 0.0],
                               atol=1E-8)

    assert src == {'source': [
        {'name': 'source1',
         'temporal_type': 'ricker',
         'center_frequency': 0.0005,
         'location': src_location,
         'spatial_type': 'vector',
         'scale': expected}]}


@pytest.mark.parametrize("lon, f_r, f_p, s_x, s_y, expected", [
    (0, 1, 2, 6371000.0, 0.0, [1, 2]),
    (180, 1, 2, -6371000.0, 0.0, [-1, -2]),
    (90, 1, 2, 0.0, 6371000.0, [-2, 1])
])
def test_2d_force_source(tmpdir, lon, f_r, f_p, s_x, s_y, expected):
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        start_time=0.0,
        end_time=10.0,
        polynomial_order=4,
        verbose=True,
        dimensions=2)

    source = salvus_seismo.ForceSource(
        longitude=lon,
        depth_in_m=0.0,
        # Up
        f_r=f_r,
        # East
        f_p=f_p,
        origin_time=obspy.UTCDateTime(2011, 1, 2, 3, 4, 5),
        sliprate="ricker")

    folder = os.path.join(str(tmpdir), "example")
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)

    # Now open the file and check it.
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()

    assert cli.startswith("salvus")

    attrs = __parse_cmdline(cli)

    assert int(attrs["dimension"]) == 2
    assert attrs["mesh-file"] == "example_mesh.e"
    assert attrs["model-file"] == "example_mesh.e"
    assert int(attrs["polynomial-order"]) == 4

    assert float(attrs["start-time"]) == 0.0
    assert float(attrs["end-time"]) == 10.0

    assert attrs["receiver-toml"] == os.path.join(folder, "receivers.toml")
    assert attrs["source-toml"] == os.path.join(folder, "source.toml")
    assert attrs["receiver-fields"] == "u_ELASTIC"
    assert attrs["receiver-file-name"] == "receiver.h5"

    # Test source toml.
    with open(os.path.join(folder, "source.toml"), "rt") as fh:
        src = toml.loads(fh.read())

    src_location = (src['source'][0]['location'])
    np.testing.assert_allclose(src_location, [s_x, s_y],
                               atol=1E-8)

    assert src == {'source': [
        {'name': 'source1',
         'temporal_type': 'ricker',
         'center_frequency': 0.0005,
         'location': src_location,
         'spatial_type': 'vector',
         'scale': expected}]}


@pytest.mark.parametrize("lon, m_rr, m_pp, m_rp, s_x, s_y, expected", [
    (0, 1, 2, 0, 6371000.0, 0.0, [1, 2, 0]),
    (180, 1, 2, 0, -6371000.0, 0.0, [1, 2, 0]),
    (90, 1, 2, 0, 0.0, 6371000.0, [2, 1, 0]),
    (270, 1, 2, 0, 0.0, -6371000.0, [2, 1, 0])
])
def test_2d_moment_tensor_source(tmpdir, lon, m_rr, m_pp, m_rp, s_x, s_y,
                                 expected):
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        start_time=0.0,
        end_time=10.0,
        polynomial_order=4,
        verbose=True,
        dimensions=2)

    source = salvus_seismo.Source(
        longitude=lon,
        depth_in_m=0.0,
        m_rr=m_rr,
        m_pp=m_pp,
        m_rp=m_rp,
        origin_time=obspy.UTCDateTime(2011, 1, 2, 3, 4, 5),
        sliprate="ricker")

    folder = os.path.join(str(tmpdir), "example")
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)

    # Now open the file and check it.
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()

    assert cli.startswith("salvus")

    attrs = __parse_cmdline(cli)

    assert int(attrs["dimension"]) == 2
    assert attrs["mesh-file"] == "example_mesh.e"
    assert attrs["model-file"] == "example_mesh.e"
    assert float(attrs["start-time"]) == 0.0
    assert float(attrs["end-time"]) == 10.0
    assert int(attrs["polynomial-order"]) == 4

    assert float(attrs["start-time"]) == 0.0
    assert float(attrs["end-time"]) == 10.0

    assert attrs["receiver-toml"] == os.path.join(folder, "receivers.toml")
    assert attrs["source-toml"] == os.path.join(folder, "source.toml")
    assert attrs["receiver-fields"] == "u_ELASTIC"
    assert attrs["receiver-file-name"] == "receiver.h5"

    # Test source toml.
    with open(os.path.join(folder, "source.toml"), "rt") as fh:
        src = toml.loads(fh.read())

    src_location = (src['source'][0]['location'])
    src_scale = (src['source'][0]['scale'])

    np.testing.assert_allclose(src_location, [s_x, s_y],
                               atol=1E-8)
    np.testing.assert_allclose(src_scale, expected,
                               atol=1E-8)

    assert src == {'source': [
        {'name': 'source1',
         'temporal_type': 'ricker',
         'center_frequency': 0.0005,
         'location': src_location,
         'spatial_type': 'moment_tensor',
         'scale': src_scale}]}


@pytest.mark.parametrize(
    "lat, lon, m_rr, m_tt, m_pp, s_x, s_y, s_z, expected", [
        (0, 0, 1, 2, 3, 6371000.0, 0.0, 0.0, [1, 3, 2, 0, 0, 0]),
        (0, 180, 1, 2, 3, -6371000.0, 0.0, 0.0, [1, 3, 2, 0, 0, 0]),
        (0, 90, 1, 2, 3, 0.0, 6371000.0, 0.0, [3, 1, 2, 0, 0, 0]),
        (0, 270, 1, 2, 3, 0.0, -6371000.0, 0.0, [3, 1, 2, 0, 0, 0])])
def test_3d_moment_tensor_source(tmpdir, lat, lon, m_rr, m_tt, m_pp, s_x, s_y,
                                 s_z, expected):
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        salvus_call="salvus_random",
        start_time=0.0,
        end_time=10.0,
        polynomial_order=4,
        verbose=True,
        dimensions=3)

    source = salvus_seismo.Source(
        latitude=lat,
        longitude=lon,
        depth_in_m=0.0,
        m_rr=m_rr,
        m_tt=m_tt,
        m_pp=m_pp,
        origin_time=obspy.UTCDateTime(2011, 1, 2, 3, 4, 5),
        sliprate="heaviside")

    folder = os.path.join(str(tmpdir), "example")
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)

    # Now open the file and check it.
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()

    assert cli.startswith("salvus_random")

    attrs = __parse_cmdline(cli)

    assert int(attrs["dimension"]) == 3
    assert attrs["mesh-file"] == "example_mesh.e"
    assert attrs["model-file"] == "example_mesh.e"
    assert float(attrs["start-time"]) == 0.0
    assert float(attrs["end-time"]) == 10.0
    assert int(attrs["polynomial-order"]) == 4

    assert float(attrs["start-time"]) == 0.0
    assert float(attrs["end-time"]) == 10.0

    assert attrs["receiver-fields"] == "u_ELASTIC"
    assert attrs["receiver-file-name"] == "receiver.h5"

    assert attrs["receiver-toml"] == os.path.join(folder, "receivers.toml")
    assert attrs["source-toml"] == os.path.join(folder, "source.toml")

    with open(os.path.join(folder, "source.toml"), "rt") as fh:
        src = toml.loads(fh.read())

    src_location = (src['source'][0]['location'])
    src_scale = (src['source'][0]['scale'])

    np.testing.assert_allclose(src_location, [s_x, s_y, s_z],
                               atol=1E-8)
    np.testing.assert_allclose(src_scale, expected,
                               atol=1E-8)

    assert src == {'source': [
        {'name': 'source1',
         'temporal_type': 'heaviside',
         'location': src_location,
         'spatial_type': 'moment_tensor',
         'scale': src_scale}]}


def test_movie_generation_cli(tmpdir):
    source = salvus_seismo.Source()

    # No movie.
    folder = os.path.join(str(tmpdir), "example_1")
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        end_time=10.0,
        dimensions=3)
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()
    assert cli.startswith("salvus")
    assert "--save-movie" not in cli
    assert "--movie-file-name" not in cli
    assert "--movie-fields" not in cli

    # Movie.
    folder = os.path.join(str(tmpdir), "example_2")
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        movie_file_name="out.h5",
        movie_fields=["u_ELASTIC"],
        end_time=10.0,
        dimensions=3)
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()
    assert cli.startswith("salvus")
    assert " --save-movie" in cli
    assert " --movie-file-name" in cli
    assert " --movie-fields" in cli


def test_absorbing_boundaries(tmpdir):
    source = salvus_seismo.Source()

    # No absorbing boundaries.
    folder = os.path.join(str(tmpdir), "example_1")
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        end_time=10.0,
        dimensions=3)
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()

    assert cli.startswith("salvus")
    attrs = __parse_cmdline(cli)
    assert "absorbing-boundaries" not in attrs

    # Absorbing boundaries.
    folder = os.path.join(str(tmpdir), "example_2")
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        end_time=10.0,
        absorbing_boundaries=("a", "b"),
        dimensions=3)
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()

    assert cli.startswith("salvus")
    attrs = __parse_cmdline(cli)
    assert "absorbing-boundaries" in attrs
    assert attrs["absorbing-boundaries"] == "a,b"


def test_wavefield_output(tmpdir):
    source = salvus_seismo.Source()

    # No wavefield output.
    folder = os.path.join(str(tmpdir), "example_1")
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        end_time=10.0,
        dimensions=3)
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()
    assert cli.startswith("salvus")
    assert "--save-fields" not in cli
    assert "--save-wavefield-file" not in cli

    # With wavefield output.
    folder = os.path.join(str(tmpdir), "example_2")
    c = salvus_seismo.Config(
        mesh_file="example_mesh.e",
        end_time=10.0,
        dimensions=3,
        wavefield_file_name="out.h5",
        wavefield_fields=["u_ELASTIC", "v_ELASTIC"])
    salvus_seismo.generate_cli_call(
        source=source,
        receivers=[],
        config=c,
        output_folder=folder)
    with open(os.path.join(folder, "run_salvus.sh"), "rt") as fh:
        cli = fh.read()
    assert cli.startswith("salvus")
    assert "--save-fields u_ELASTIC,v_ELASTIC" in cli
    assert "--save-wavefield-file out.h5" in cli
