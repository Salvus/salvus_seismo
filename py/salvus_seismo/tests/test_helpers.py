#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
:copyright:
    Lion Krischer (lionkrischer@gmail.com), 2017
:license:
    MIT License
'''
import numpy as np
import pytest

import salvus_seismo.helpers


@pytest.mark.parametrize("lat, lon, input, output", [
    (0, 0, [1, 0, 0], [1, 0, 0]),
    (0, 0, [0, 1, 0], [0, 0, 1]),
    (0, 0, [0, 0, 1], [0, 1, 0]),
    (90, 0, [1, 0, 0], [0, -1, 0]),
    (90, 0, [0, 1, 0], [0, 0, 1]),
    (90, 0, [0, 0, 1], [1, 0, 0]),
    (0, 90, [1, 0, 0], [0, 0, -1]),
    (0, 90, [0, 1, 0], [1, 0, 0]),
    (0, 90, [0, 0, 1], [0, 1, 0])
])
def test_rotation_matrix_3d(lat, lon, input, output):
    # The matrix should go from x, y, z in ZNE.
    M = salvus_seismo.helpers.get_transformation_matrix_3d(latitude=lat,
                                                           longitude=lon)
    np.testing.assert_allclose(np.dot(M, input), output, atol=1E-8)

    # Also test the inverse.
    M_inv = np.linalg.inv(M)
    np.testing.assert_allclose(np.dot(M_inv, np.dot(M, input)), input,
                               atol=1E-8)


@pytest.mark.parametrize("input, expected", [
    ("u_ELASTIC", ["u_ELASTIC"]),
    ("u_ELASTIC,u_ACOUSTIC", ["u_ELASTIC", "u_ACOUSTIC"]),
    (["u_ELASTIC", "u_ACOUSTIC"], ["u_ELASTIC", "u_ACOUSTIC"])
])
def test_string_to_list(input, expected):
    assert salvus_seismo.helpers.string_to_list(input) == expected
