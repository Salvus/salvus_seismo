#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
:copyright:
    Lion Krischer (lionkrischer@gmail.com), 2017
:license:
    MIT License
'''
from obspy.core.inventory import Inventory, Network, Station, Channel
import salvus_seismo


def test_receiver_from_inventory():
    inv = Inventory(source="", networks=[])
    net = Network(code="XX", stations=[])
    sta = Station(code="ABC", latitude=0.0, longitude=2.0, elevation=3.0)
    cha = Channel(code="HHZ", location_code="00",
                  latitude=0.0, longitude=3.0, elevation=4.0,
                  depth=5.0)

    # First example don't add a channel - it will use the station coordinates.
    inv.networks.append(net)
    net.stations.append(sta)

    receivers = salvus_seismo.Receiver.parse(inv)
    expected = salvus_seismo.Receiver(
        network="XX", station="ABC", location="",
        latitude=0.0, longitude=2.0, depth_in_m=0.0)
    assert receivers == [expected]

    # Now append the channel and it should use those coordinates.

    sta.channels.append(cha)
    receivers = salvus_seismo.Receiver.parse(inv)
    expected = salvus_seismo.Receiver(
        network="XX", station="ABC", location="",
        latitude=0.0, longitude=3.0, depth_in_m=5.0)
    assert receivers == [expected]
