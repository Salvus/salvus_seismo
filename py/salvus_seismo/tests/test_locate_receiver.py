#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for the ReceiverLocator class.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2017
:license:
    None
'''
import numpy as np
from scipy.spatial import ConvexHull

from salvus_seismo.locate_receiver import ReceiverLocator


def test_locate_receiver_tri():
    np.random.seed(1232)

    # generate test data
    n = 100
    X = np.random.randn(n * 3).reshape((n, 3))
    norm = (X ** 2).sum(axis=1) ** 0.5
    for i in np.arange(3):
        X[:, i] /= norm

    X[0, :] = 0.0

    qhull = ConvexHull(X)

    points = qhull.points
    connectivity = qhull.simplices

    rloc = ReceiverLocator([points, connectivity])

    # random search point
    p = np.random.randn(3)
    p /= (p ** 2).sum() ** 0.5

    ielem = rloc.search_idx(p)
    assert ielem == 52


def test_locate_receiver_quad():
    np.random.seed(1232)

    # generate test data
    n = 100
    X = np.random.randn(n * 3).reshape((n, 3))
    norm = (X ** 2).sum(axis=1) ** 0.5
    for i in np.arange(3):
        X[:, i] /= norm

    X[0, :] = 0.0

    qhull = ConvexHull(X)

    points = qhull.points
    connectivity = qhull.simplices
    connectivity = connectivity[:, [0, 1, 2, 1]]

    rloc = ReceiverLocator([points, connectivity])

    # random search point
    p = np.random.randn(3)
    p /= (p ** 2).sum() ** 0.5

    ielem = rloc.search_idx(p)
    assert ielem == 52
