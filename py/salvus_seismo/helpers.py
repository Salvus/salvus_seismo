import math

import numpy as np


def string_to_list(obj):
    """
    Turns a comma separated string into a list of strings (if obj is a string).
    If obj is not a string, simply return.

    :param obj: String or list to transform
    """
    if isinstance(obj, str):
        return obj.split(',')
    return obj


def elliptic_to_geocentric_latitude(lat, axis_a=6378137.0,
                                    axis_b=6356752.314245):
    """
    Convert a latitude defined on an ellipsoid to a geocentric one.

    :param lat: The latitude to convert.
    :param axis_a: The length of the major axis of the planet. Defaults to
        the value of the WGS84 ellipsoid.
    :param axis_b: The length of the minor axis of the planet. Defaults to
        the value of the WGS84 ellipsoid.

    >>> elliptic_to_geocentric_latitude(0.0)
    0.0
    >>> elliptic_to_geocentric_latitude(90.0)
    90.0
    >>> elliptic_to_geocentric_latitude(-90.0)
    -90.0
    >>> elliptic_to_geocentric_latitude(45.0)
    44.80757678401642
    >>> elliptic_to_geocentric_latitude(-45.0)
    -44.80757678401642
    """
    _f = (axis_a - axis_b) / axis_a
    E_2 = 2 * _f - _f ** 2

    # Singularities close to the pole and the equator. Just return the value
    # in that case.
    if abs(lat) < 1E-6 or abs(lat - 90) < 1E-6 or \
            abs(lat + 90.0) < 1E-6:
        return lat

    return math.degrees(math.atan((1 - E_2) * math.tan(math.radians(lat))))


def get_transformation_matrix_3d(latitude, longitude):
    """
    Returns the transformation matrix that converts x, y, z in Salvus
    coordinates to ZNE in physical coordinates.

    Assumes x points through Lat/Lng 0/0, y through 0/90, and the z through the
    North pole.

    Multiplying the matrix with a [x, y, z].T will yield a vector with
    [Z, N, E].T.
    """
    theta = np.deg2rad(90 - latitude)
    phi = np.deg2rad(longitude)

    M = np.array([
        # r unit vector in Cartesian coordinates.
        [np.sin(theta) * np.cos(phi),
         np.sin(theta) * np.sin(phi),
         np.cos(theta)],
        # theta unit vector
        [-np.cos(theta) * np.cos(phi),
         -np.cos(theta) * np.sin(phi),
         np.sin(theta)],
        # phi unit vector
        [-np.sin(phi), np.cos(phi), 0]
    ])

    return M


def get_transformation_matrix_2d(longitude):
    """
    2D version of get_transformation_matrix_3d.
    """
    phi = np.deg2rad(longitude)

    M = np.array([
        # r unit vector in Cartesian coordinates.
        [np.cos(phi), np.sin(phi)],
        # phi unit vector
        [-np.sin(phi), np.cos(phi)]
    ])

    return M


def angle_between_vecs(vec_a, vec_b):
    """
    Compute the angle between two vectors in 2D or 3D

    Uses this stable formula:
        angle = atan2(norm(cross(a,b)), dot(a,b))

    see also:
    https://ch.mathworks.com/matlabcentral/answers/16243-angle-between-two-vectors-in-3d  # NoQa

    :param vec_a, vec_b: vectors to compute the angle between
    :type vec_a, vec_b: numpy array or float, shape[-1] should be 2 or 3

    :returns: angle between vec_a and vec_b, either float or numpy array
        with shape[:-1] depending on input
    """
    ndim = vec_a.shape[-1]

    cross = np.cross(vec_a, vec_b)
    if ndim == 2:
        cross = np.abs(cross)
    elif ndim == 3:
        cross = np.sum(cross ** 2, axis=-1) ** 0.5

    dot = np.sum(vec_a * vec_b, axis=-1)
    theta = np.arctan2(cross, dot)

    return theta
