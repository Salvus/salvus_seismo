#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
A class to find receivers on surfaces of exodus meshes.

:copyright:
    Martin van Driel (Martin@vanDriel.de)
    Lion Krischer (lion.krischer@gmail.com), 2017
:license:
    None as of now.
'''
from __future__ import print_function

import time

import numpy as np
from scipy.spatial import cKDTree
from matplotlib.tri import Triangulation
import matplotlib.pyplot as plt

from pyexodus import exodus
from salvus_wave.shape_mappings import HexP1, QuadP1

from .helpers import angle_between_vecs


# 1-based indexing for the exodus sides, 0-based for the salvus sides.
_HEX_EXODUS_TO_SALVUS_SIDE_MAPPING = {
    # Front
    1: 2,
    # Right
    2: 4,
    # Back
    3: 3,
    # Left
    4: 5,
    # Bottom
    5: 0,
    # Top
    6: 1
}

# 1-based indexing for the exodus sides, 0-based for the salvus sides.
_QUAD_EXODUS_TO_SALVUS_EDGE_MAPPING = {
    # Bottom
    1: 0,
    # Right
    2: 2,
    # Top
    3: 1,
    # Left
    4: 3,
}


class ReceiverLocator(object):
    def __init__(self, exodus_file, side_set_name="r1"):
        """
        Class to locate receivers on the surface of exodus meshes.

        Expensive initialization so best only do it once.

        :type exodus_file: str
        :param exodus_file: Path to the exodus file with the mesh.
        :type side_set_name: str
        :param side_set_name: Side set name to consider. ``"r1"`` is usually
            the free surface.
        """
        # Directly use the points and connectivity - mainly interesting for
        # testing.
        if not isinstance(exodus_file, str):
            points = exodus_file[0]
            connectivity = exodus_file[1]
            self._e = None
        # Open the file.
        else:
            print("Retrieving surface information from exodus file ... ",
                  end="", flush=True)
            _start = time.time()
            self._e = exodus(exodus_file, mode='r')
            # get nodes and elements from the mesh, r1 is the free surface
            idx = self._e.get_side_set_ids()[
                self._e.get_side_set_names().index(side_set_name)]
            node_count, nodes = self._e.get_side_set_node_list(idx)
            self.elements, self.sides = self._e.get_side_set(idx)

            if not np.all(node_count == 4):
                raise ValueError('No Quad? Currently only supports hex '
                                 'meshes.')

            # build connectivity starting from index 0
            unique_nodes, node_map = np.unique(nodes, return_inverse=True)
            connectivity = node_map.reshape((self.elements.shape[0], 4))

            # This pretty slow but still faster then getting ever node
            # separately. There is probably some speedup potential to use the
            # chunked loader like in instaseis.
            # points = np.array(self._e.get_coord(unique_nodes)).T

            # Faster alternative: read all points and then slice to the ones
            # actually needed. Might run into memor issues for large meshes.
            points = np.array(self._e.get_coords())[:, unique_nodes-1].T
            _end = time.time()
            print("[DONE in %.2f seconds]" % (_end - _start))

        if not points.shape[1] == 3:
            raise ValueError('only works in 3D')

        if connectivity.shape[1] not in [3, 4]:
            raise ValueError('only works for triangles and quads')

        self.points = points

        self.quad = connectivity.shape[1] == 4
        if not self.quad:
            self.connectivity = connectivity
        else:
            c1 = connectivity[:, [0, 1, 2]]
            c2 = connectivity[:, [2, 3, 0]]
            self.connectivity = np.insert(c2, np.arange(c1.shape[0]), c1,
                                          axis=0)
        p = np.zeros((self.connectivity.shape[0], 3))
        for i in range(3):
            p += self.points[self.connectivity[:, i]]

        norm = (p ** 2).sum(axis=1) ** 0.5
        for i in range(3):
            p[:, i] /= norm
        self.tree = cKDTree(p)

    def __del__(self):
        try:
            self._e.close()
        except Exception:
            pass

    def inside_simplex(self, ielem, p, epsilon=1e-10):
        """
        Möller–Trumbore intersection algorithm in pure python
        Based on
        http://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm  # NoQa

        Modified from https://github.com/kliment/Printrun/blob/master/printrun/stltool.py  # NoQa

        GPL! Must be rewritten or at least altered.
        """
        v1 = self.points[self.connectivity[ielem, 0]]
        v2 = self.points[self.connectivity[ielem, 1]]
        v3 = self.points[self.connectivity[ielem, 2]]

        e1 = v2 - v1
        e2 = v3 - v1

        pvec = np.cross(p, e2)
        det = e1.dot(pvec)

        if abs(det) < epsilon:
            return False

        inv_det = 1. / det
        tvec = - v1
        u = tvec.dot(pvec) * inv_det

        if u < 0. or u > 1.:
            return False

        qvec = np.cross(tvec, e1)
        v = p.dot(qvec) * inv_det

        if v < 0. or u + v > 1.:
            return False

        t = e2.dot(qvec) * inv_det

        if t < epsilon:
            return False

        return True

    def search(self, p):
        idx = self.search_idx(p=p)
        elem_id = self.elements[idx]
        side_id = self.sides[idx]
        nodes = self._e.get_elem_connectivity(id=1, indices=[elem_id])[0][0]
        assert len(nodes) == 8, "Must be hexes for now!"

        vertices = np.zeros((8, 3))
        # Map the exodus index order to the one used by salvus.
        order = (0, 3, 2, 1, 4,	5, 6, 7)
        # _i is order salvus, _j in exodus.
        for _i, _j in enumerate(order):
            x, y, z = self._e.get_coord(nodes[_j])
            vertices[_i, 0] = x
            vertices[_i, 1] = y
            vertices[_i, 2] = z

        surface_point = HexP1.get_intersection_with_facet(
            vertices=vertices,
            side_index=_HEX_EXODUS_TO_SALVUS_SIDE_MAPPING[side_id],
            line_a=np.zeros(3),
            line_b=p)

        assert HexP1.check_hull(surface_point, vertices)
        return surface_point

    def search_idx(self, p, nneighbour=10):
        neighbours = self.tree.query(p, k=nneighbour)[1]
        for ielem in neighbours:
            ielem = int(ielem)
            if self.inside_simplex(ielem, p):
                return ielem // 2 if self.quad else ielem

        raise RuntimeError('element not found')

    def plot(self, ielem=None, p=None):
        # plot triangulation
        x, y, z = self.points.T
        norm = (x ** 2 + y ** 2 + z ** 2) ** 0.5
        x /= norm
        y /= norm
        z /= norm
        tria = Triangulation(x, y, triangles=self.connectivity)
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_aspect("equal")
        ax.plot_trisurf(tria, z, color='w', shade=False)

        if ielem is not None:
            if self.quad:
                x3, y3, z3 = self.points[self.connectivity[ielem * 2]].T * 1.1
                ax.scatter(x3, y3, z3, c='b', s=100)
                x3, y3, z3 = self.points[
                    self.connectivity[ielem * 2 + 1]].T * 1.1
                ax.scatter(x3, y3, z3, c='b', s=100)
            else:
                x3, y3, z3 = self.points[self.connectivity[ielem]].T * 1.1
                ax.scatter(x3, y3, z3, c='b', s=100)

        if p is not None:
            x4, y4, z4 = p * 1.1 / (p ** 2).sum() ** 0.5
            ax.scatter(x4, y4, z4, c='r', s=100)

        plt.show()


class ReceiverLocator2D(object):
    def __init__(self, exodus_file, side_set_name="r1"):
        """
        Class to locate receivers on the surface of 2D exodus meshes in the XY
        plane.

        :type exodus_file: str
        :param exodus_file: Path to the exodus file with the mesh.
        :type side_set_name: str
        :param side_set_name: Side set name to consider. ``"r1"`` is usually
            the free surface.
        """
        # Directly use the points and connectivity - mainly interesting for
        # testing.
        if not isinstance(exodus_file, str):
            points = exodus_file[0]
            connectivity = exodus_file[1]
            self._e = None
        # Open the file.
        else:
            self._e = exodus(exodus_file, mode='r')
            # get nodes and elements from the mesh, r1 is the free surface
            idx = self._e.get_side_set_ids()[
                self._e.get_side_set_names().index(side_set_name)]
            node_count, nodes = self._e.get_side_set_node_list(idx)
            self.elements, self.sides = self._e.get_side_set(idx)

            if not np.all(node_count == 2):
                raise ValueError('No Edge?')

            # build connectivity starting from index 0
            unique_nodes, node_map = np.unique(nodes, return_inverse=True)
            connectivity = node_map.reshape((self.elements.shape[0], 2))

            # This pretty slow but still faster then getting ever node
            # separately. There is probably some speedup potential to use the
            # chunked loader like in instaseis.
            points = np.array(self._e.get_coord(unique_nodes)).T

        if not np.all(points[:, -1] == 0.):
            raise ValueError('only works in 3D')

        if not connectivity.shape[1] == 2:
            raise ValueError('only works for edges')

        self.angle = angle_between_vecs(points[connectivity[:, 0]],
                                        points[connectivity[:, 1]])

        self.points = points[:, :2]
        self.connectivity = connectivity

    def __del__(self):
        try:
            self._e.close()
        except Exception:
            pass

    def search_idx(self, p):
        if len(p.shape) == 1:
            pvec = p[np.newaxis, :]
        else:
            pvec = p

        angle_1 = angle_between_vecs(
            pvec, self.points[self.connectivity[:, 0]])
        angle_2 = angle_between_vecs(
            pvec, self.points[self.connectivity[:, 1]])

        idx = np.where(np.logical_and(angle_1 <= self.angle,
                                      angle_2 <= self.angle))

        if len(idx[0]) == 0:
            raise ValueError('Element not found')

        # choosing the first one in case it is on the boundary
        return idx[0][0]

    def search(self, p):
        p = p[:2]
        idx = self.search_idx(p=p)
        elem_id = self.elements[idx]
        side_id = self.sides[idx]
        nodes = self._e.get_elem_connectivity(id=1, indices=[elem_id])[0][0]
        assert len(nodes) == 4, "Must be quads for now!"

        vertices = np.zeros((4, 2))
        # Map the exodus index order to the one used by salvus.
        for _i in range(4):
            x, y, z = self._e.get_coord(nodes[_i])
            vertices[_i, 0] = x
            vertices[_i, 1] = y

        surface_point = QuadP1.get_intersection_with_facet(
            vertices=vertices,
            side_index=_QUAD_EXODUS_TO_SALVUS_EDGE_MAPPING[side_id],
            line_a=np.zeros(2),
            line_b=p[:2])

        assert QuadP1.check_hull(surface_point, vertices)
        return surface_point
