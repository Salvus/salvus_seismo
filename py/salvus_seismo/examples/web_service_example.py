from obspy.clients.fdsn import Client
import salvus_seismo

c_usgs = Client("USGS")
c_iris = Client("IRIS")

c = salvus_seismo.Config(
    mesh_file="example_mesh.e",
    duration=2.0,
    time_step=1e-3,
    polynomial_order=4,
    verbose=True)

# Make sure to get an event with a moment tensor!
event = c_usgs.get_events(eventid="us10007c4w")[0]
event_time = event.preferred_origin().time
# For now you have to parse the source to specify the source time function.
source = salvus_seismo.Source.parse(event, sliprate="ricker")

# Use all IU stations as the receivers.
receivers = salvus_seismo.Receiver.parse(
    c_iris.get_stations(
        network="IU",
        starttime=event_time - 10,
        endtime=event_time + 10))

salvus_seismo.generate_cli_call(
    source=source,
    receivers=receivers,
    config=c,
    output_folder="salvus_webservice_example")
