import obspy
import salvus_seismo

c = salvus_seismo.Config(
    mesh_file="example_mesh.e",
    duration=2.0,
    time_step=1e-3,
    polynomial_order=4,
    verbose=True)

receiver = salvus_seismo.Receiver(
    latitude=42.6390,
    longitude=74.4940,
    network="AB",
    station="CED",
    location="")

source = salvus_seismo.Source(
    latitude=89.91,
    longitude=0.0,
    depth_in_m=12000,
    m_rr=4.710000e+24 / 1E7,
    m_tt=3.810000e+22 / 1E7,
    m_pp=-4.740000e+24 / 1E7,
    m_rt=3.990000e+23 / 1E7,
    m_rp=-8.050000e+23 / 1E7,
    m_tp=-1.230000e+24 / 1E7,
    origin_time=obspy.UTCDateTime(2011, 1, 2, 3, 4, 5),
    sliprate="ricker")

salvus_seismo.generate_cli_call(
    source=source,
    receivers=[receiver],
    config=c,
    output_folder="rundir")
