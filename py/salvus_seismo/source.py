#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Source and Receiver classes of Salvus.

Strongly inspired by the source and receiver handling in Instaseis. Currently
only supports a spherical planet and it should not yet be considered stable and
will likely evolve quite a bit in the future.

:copyright:
    Lion Krischer (krischer@geophysik.uni-muenchen.de), 2014-2016
    Martin van Driel (Martin@vanDriel.de), 2014-2016
:license:
    Mozilla Public License Version 2.0
    (https://www.mozilla.org/en-US/MPL/2.0/)
"""
from __future__ import (absolute_import, division, print_function)

import functools
import numpy as np
import obspy
import obspy.core.inventory
import os

from .exceptions import ReceiverParseError, SourceParseError
from .helpers import elliptic_to_geocentric_latitude


def _purge_duplicates(f):
    """
    Simple decorator removing duplicates in the returned list. Preserves the
    order and will remove duplicates occuring later in the list.
    """
    @functools.wraps(f)
    def wrapper(*args, **kwds):
        ret_val = f(*args, **kwds)
        new_list = []
        for item in ret_val:
            if item in new_list:
                continue
            new_list.append(item)
        return new_list
    return wrapper


def _purge_duplicate_stations(f):
    @functools.wraps(f)
    def wrapper(*args, **kwds):
        ret_val = f(*args, **kwds)
        new_list = []
        stations = []
        for item in ret_val:
            sta = "%s.%s" % (item.network, item.station)
            if sta in stations:
                continue
            stations.append(sta)
            new_list.append(item)
        return new_list
    return wrapper


def moment2magnitude(M0):
    """
    Convert seismic moment M0 to moment magnitude Mw

    :param M0: seismic moment in Nm
    :type M0: float
    :return Mw: moment magnitude
    :type Mw: float
    """
    return 2.0 / 3.0 * np.log10(M0) - 6.0


def magnitude2moment(Mw):
    """
    Convert moment magnitude Mw to seismic moment M0

    :param Mw: moment magnitude
    :type Mw: float
    :return M0: seismic moment in Nm
    :type M0: float
    """
    return 10.0 ** ((Mw + 6.0) / 2.0 * 3.0)


class SourceOrReceiver(object):
    def __init__(self, latitude=0.0, longitude=0.0, depth_in_m=0.0):
        self.latitude = float(latitude)
        self.longitude = float(longitude)
        self.depth_in_m = float(depth_in_m)
        assert self.depth_in_m >= 0.0

        if not (-90 <= self.latitude <= 90):
            raise ValueError("Invalid latitude value. Latitude must be "
                             "-90 <= x <= 90.")

    def __eq__(self, other):
        if type(self) != type(other):
            return False
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def colatitude(self):
        return 90.0 - self.latitude

    @property
    def colatitude_rad(self):
        return np.deg2rad(90.0 - self.latitude)

    @property
    def longitude_rad(self):
        return np.deg2rad(self.longitude)

    @property
    def latitude_rad(self):
        return np.deg2rad(self.latitude)

    def radius_in_m(self, planet_radius=6371e3):
        return planet_radius - self.depth_in_m

    def x(self, planet_radius=6371e3):
        return np.cos(np.deg2rad(self.latitude)) * \
            np.cos(np.deg2rad(self.longitude)) * \
            self.radius_in_m(planet_radius=planet_radius)

    def y(self, planet_radius=6371e3):
        return np.cos(np.deg2rad(self.latitude)) * \
            np.sin(np.deg2rad(self.longitude)) * \
            self.radius_in_m(planet_radius=planet_radius)

    def z(self, planet_radius=6371e3):
        return np.sin(np.deg2rad(self.latitude)) * \
            self.radius_in_m(planet_radius=planet_radius)


class Source(SourceOrReceiver):
    """
    Class to handle a seismic moment tensor source including a source time
    function.
    """
    def __init__(self, latitude=0.0, longitude=0.0, depth_in_m=0.0,
                 m_rr=0.0, m_tt=0.0, m_pp=0.0,
                 m_rt=0.0, m_rp=0.0, m_tp=0.0,
                 sliprate="ricker",
                 center_frequency=0.0005,
                 origin_time=obspy.UTCDateTime(0)):
        """
        :param latitude: geocentric latitude of the source in degree
        :param longitude: longitude of the source in degree
        :param depth_in_m: source depth in m
        :param m_rr: moment tensor components in r, theta, phi in Nm
        :param m_tt: moment tensor components in r, theta, phi in Nm
        :param m_pp: moment tensor components in r, theta, phi in Nm
        :param m_rt: moment tensor components in r, theta, phi in Nm
        :param m_rp: moment tensor components in r, theta, phi in Nm
        :param m_tp: moment tensor components in r, theta, phi in Nm
        :param sliprate: Name of the source time function or normalized source
            time function (sliprate). Currently only supports "ricker" as an
            argument.
        :param center_frequency: Center frequency of the source wavelet.
        :param origin_time: The origin time of the source. Defined as the peak
            of the source time function.

        >>> import salvus_seismo
        >>> source = salvus_seismo.Source(
        ...     latitude=89.91, longitude=0.0, depth_in_m=12000,
        ...     m_rr = 4.71e+17, m_tt = 3.81e+15, m_pp =-4.74e+17,
        ...     m_rt = 3.99e+16, m_rp =-8.05e+16, m_tp =-1.23e+17)
        >>> print(source)  # doctest: +NORMALIZE_WHITESPACE
        Salvus Source:
            Origin Time      : 1970-01-01T00:00:00.000000Z
            Longitude        :    0.0 deg
            Latitude         :   89.9 deg
            Depth            : 1.2e+01 km km
            Moment Magnitude :   5.80
            Scalar Moment    :   4.96e+17 Nm
            Mrr              :   4.71e+17 Nm
            Mtt              :   3.81e+15 Nm
            Mpp              :  -4.74e+17 Nm
            Mrt              :   3.99e+16 Nm
            Mrp              :  -8.05e+16 Nm
            Mtp              :  -1.23e+17 Nm
        """
        super(Source, self).__init__(latitude, longitude, depth_in_m)
        self.m_rr = m_rr
        self.m_tt = m_tt
        self.m_pp = m_pp
        self.m_rt = m_rt
        self.m_rp = m_rp
        self.m_tp = m_tp
        self.origin_time = origin_time

        supported_stfs = ["ricker", "heaviside"]
        if sliprate.lower() not in supported_stfs:
            raise ValueError("Currently only 'ricker' and 'heaviside' "
                             "are supported as a source time function.")
        self.sliprate = sliprate.lower()
        self.center_frequency = float(center_frequency)

    @staticmethod
    def parse(filename_or_obj, sliprate="ricker"):
        """
        Attempts to parse anything to a Source object. Can currently read
        anything ObsPy can read, ObsPy event related objects.

        For anything ObsPy related, it must contain a full moment tensor,
        otherwise it will raise an error.

        Coordinates are assumed to be defined on the WGS84 ellipsoid and
        will be converted to geocentric coordinates.

        :param filename_or_obj: The object or filename to parse.


        The following example will read a local QuakeML file and return a
        :class:`~salvus_seismo.source.Source` object.

        >>> import salvus_seismo
        >>> source = salvus_seismo.Source.parse(quakeml_file)
        >>> print(source)  # doctest: +NORMALIZE_WHITESPACE
        Salvus Source:
            Origin Time      : 2010-03-24T14:11:31.000000Z
            Longitude        :   40.1 deg
            Latitude         :   38.6 deg
            Depth            : 4.5e+00 km km
            Moment Magnitude :   5.15
            Scalar Moment    :   5.37e+16 Nm
            Mrr              :   5.47e+15 Nm
            Mtt              :  -4.11e+16 Nm
            Mpp              :   3.56e+16 Nm
            Mrt              :   2.26e+16 Nm
            Mrp              :  -2.25e+16 Nm
            Mtp              :   1.92e+16 Nm
        """
        # py2/py3 compatibility.
        try:  # pragma: no cover
            str_types = (str, bytes, unicode)  # NOQA
        except Exception:  # pragma: no cover
            str_types = (str, bytes)

        if isinstance(filename_or_obj, str_types):
            # Anything ObsPy can read.
            try:
                src = obspy.read_events(filename_or_obj)
            except Exception:
                pass
            else:
                return Source.parse(src)
            raise SourceParseError("Could not parse the given source.")
        elif isinstance(filename_or_obj, obspy.Catalog):
            if len(filename_or_obj) == 0:
                raise SourceParseError("Event catalog contains zero events.")
            elif len(filename_or_obj) > 1:
                raise SourceParseError(
                    "Event catalog contains %i events. Only one is allowed. "
                    "Please parse seperately." % len(filename_or_obj))
            return Source.parse(filename_or_obj[0])
        elif isinstance(filename_or_obj, obspy.core.event.Event):
            ev = filename_or_obj
            if not ev.origins:
                raise SourceParseError("Event must contain an origin.")
            if not ev.focal_mechanisms:
                raise SourceParseError("Event must contain a focal mechanism.")
            org = ev.preferred_origin() or ev.origins[0]
            fm = ev.preferred_focal_mechanism() or ev.focal_mechanisms[0]
            if not fm.moment_tensor:
                raise SourceParseError("Event must contain a moment tensor.")
            t = fm.moment_tensor.tensor
            return Source(
                latitude=elliptic_to_geocentric_latitude(org.latitude),
                longitude=org.longitude,
                depth_in_m=org.depth,
                origin_time=org.time,
                m_rr=t.m_rr,
                m_tt=t.m_tt,
                m_pp=t.m_pp,
                m_rt=t.m_rt,
                m_rp=t.m_rp,
                m_tp=t.m_tp,
                sliprate=sliprate)
        else:
            raise NotImplementedError

    @classmethod
    def from_strike_dip_rake(self, latitude, longitude, depth_in_m, strike,
                             dip, rake, M0, time_shift=None, sliprate=None,
                             dt=None, origin_time=obspy.UTCDateTime(0)):
        """
        Initialize a source object from a shear source parameterized by strike,
        dip and rake.

        :param latitude: geocentric latitude of the source in degree
        :param longitude: longitude of the source in degree
        :param depth_in_m: source depth in m
        :param strike: strike of the fault in degree
        :param dip: dip of the fault in degree
        :param rake: rake of the fault in degree
        :param M0: scalar moment
        :param time_shift: correction of the origin time in seconds. only
            useful in the context of finite sources
        :param sliprate: normalized source time function (sliprate)
        :param dt: sampling of the source time function
        :param origin_time: The origin time of the source. If you don't
            reconvolve with another source time function this time is the
            peak of the source time function used to generate the database.
            If you reconvolve with another source time function this time is
            the time of the first sample of the final seismogram.

        >>> import salvus_seismo
        >>> source = salvus_seismo.Source.from_strike_dip_rake(
        ...     latitude=10.0, longitude=12.0, depth_in_m=1000, strike=79,
        ...     dip=10, rake=20, M0=1E17)
        >>> print(source)  # doctest: +NORMALIZE_WHITESPACE
        Salvus Source:
            Origin Time      : 1970-01-01T00:00:00.000000Z
            Longitude        :   12.0 deg
            Latitude         :   10.0 deg
            Depth            : 1.0e+00 km km
            Moment Magnitude :   5.33
            Scalar Moment    :   1.00e+17 Nm
            Mrr              :   1.17e+16 Nm
            Mtt              :  -1.74e+16 Nm
            Mpp              :   5.69e+15 Nm
            Mrt              :  -4.92e+16 Nm
            Mrp              :   8.47e+16 Nm
            Mtp              :   1.29e+16 Nm
        """
        assert M0 >= 0
        if dt is not None:
            assert dt > 0

        # formulas in Udias (17.24) are in geographic system North, East,
        # Down, which # transforms to the geocentric as:
        # Mtt =  Mxx, Mpp = Myy, Mrr =  Mzz
        # Mrp = -Myz, Mrt = Mxz, Mtp = -Mxy
        # voigt in tpr: Mtt Mpp Mrr Mrp Mrt Mtp
        phi = np.deg2rad(strike)
        delta = np.deg2rad(dip)
        lambd = np.deg2rad(rake)

        m_tt = (- np.sin(delta) * np.cos(lambd) * np.sin(2. * phi) -
                np.sin(2. * delta) * np.sin(phi)**2. * np.sin(lambd)) * M0

        m_pp = (np.sin(delta) * np.cos(lambd) * np.sin(2. * phi) -
                np.sin(2. * delta) * np.cos(phi)**2. * np.sin(lambd)) * M0

        m_rr = (np.sin(2. * delta) * np.sin(lambd)) * M0

        m_rp = (- np.cos(phi) * np.sin(lambd) * np.cos(2. * delta) +
                np.cos(delta) * np.cos(lambd) * np.sin(phi)) * M0

        m_rt = (- np.sin(lambd) * np.sin(phi) * np.cos(2. * delta) -
                np.cos(delta) * np.cos(lambd) * np.cos(phi)) * M0

        m_tp = (- np.sin(delta) * np.cos(lambd) * np.cos(2. * phi) -
                np.sin(2. * delta) * np.sin(2. * phi) * np.sin(lambd) / 2.) * \
            M0

        source = self(latitude, longitude, depth_in_m, m_rr, m_tt, m_pp, m_rt,
                      m_rp, m_tp, time_shift, sliprate, dt,
                      origin_time=origin_time)

        # storing strike, dip and rake for plotting purposes
        source.phi = phi
        source.delta = delta
        source.lambd = lambd

        return source

    @property
    def M0(self):
        """
        Scalar Moment M0 in Nm
        """
        return (self.m_rr ** 2 + self.m_tt ** 2 + self.m_pp ** 2 +
                2 * self.m_rt ** 2 + 2 * self.m_rp ** 2 +
                2 * self.m_tp ** 2) ** 0.5 * 0.5 ** 0.5

    @property
    def moment_magnitude(self):
        """
        Moment magnitude M_w
        """
        return moment2magnitude(self.M0)

    @property
    def tensor(self):
        """
        List of moment tensor components in r, theta, phi coordinates:
        [m_rr, m_tt, m_pp, m_rt, m_rp, m_tp]
        """
        return np.array([self.m_rr, self.m_tt, self.m_pp, self.m_rt, self.m_rp,
                         self.m_tp])

    @property
    def tensor_voigt(self):
        """
        List of moment tensor components in theta, phi, r coordinates in Voigt
        notation:
        [m_tt, m_pp, m_rr, m_rp, m_rt, m_tp]
        """
        return np.array([self.m_tt, self.m_pp, self.m_rr, self.m_rp, self.m_rt,
                         self.m_tp])

    def set_sliprate(self, sliprate, dt, time_shift=None, normalize=True):
        """
        Add a source time function (sliprate) to a initialized source object.

        :param sliprate: (normalized) sliprate
        :param dt: sampling of the sliprate
        :param normalize: if sliprate is not normalized, set this to true to
            normalize it using trapezoidal rule style integration
        """
        self.sliprate = np.array(sliprate)
        if normalize:
            self.sliprate /= np.trapz(sliprate, dx=dt)
        self.dt = dt
        self.time_shift = time_shift

    def __str__(self):
        return_str = 'Salvus Source:\n'
        return_str += '\tOrigin Time      : %s\n' % (self.origin_time,)
        return_str += '\tLongitude        : %6.1f deg\n' % (self.longitude,)
        return_str += '\tLatitude         : %6.1f deg\n' % (self.latitude,)
        return_str += '\tDepth            : %s km\n' % (
            "%6.1e km" % (self.depth_in_m / 1e3))
        return_str += '\tMoment Magnitude :   %4.2f\n' \
                      % (self.moment_magnitude,)
        return_str += '\tScalar Moment    : %10.2e Nm\n' % (self.M0,)
        return_str += '\tMrr              : %10.2e Nm\n' % (self.m_rr,)
        return_str += '\tMtt              : %10.2e Nm\n' % (self.m_tt,)
        return_str += '\tMpp              : %10.2e Nm\n' % (self.m_pp,)
        return_str += '\tMrt              : %10.2e Nm\n' % (self.m_rt,)
        return_str += '\tMrp              : %10.2e Nm\n' % (self.m_rp,)
        return_str += '\tMtp              : %10.2e Nm\n' % (self.m_tp,)

        return return_str


class ForceSource(SourceOrReceiver):
    """
    Class to handle a seismic force source.

    :param latitude: geocentric latitude of the source in degree
    :param longitude: longitude of the source in degree
    :param depth_in_m: source depth in m
    :param f_r: force components in r, theta, phi in N
    :param f_t: force components in r, theta, phi in N
    :param f_p: force components in r, theta, phi in N
    :param origin_time: The origin time of the source. If you don't
        reconvolve with another source time function this time is the
        peak of the source time function used to generate the database.
        If you reconvolve with another source time function this time is
        the time of the first sample of the final seismogram.
    :param sliprate: normalized source time function (sliprate)


    >>> import salvus_seismo
    >>> source = salvus_seismo.ForceSource(latitude=12.34, longitude=12.34,
    ...                                    f_r=1E10)
    >>> print(source)  # doctest: +NORMALIZE_WHITESPACE
    Salvus Force Source:
        Origin Time      : 1970-01-01T00:00:00.000000Z
        Longitude :   12.3 deg
        Latitude  :   12.3 deg
        Fr        :   1.00e+10 N
        Ft        :   0.00e+00 N
        Fp        :   0.00e+00 N
    """
    def __init__(self, latitude=0.0, longitude=0.0, depth_in_m=0.0,
                 f_r=0., f_t=0., f_p=0.,
                 origin_time=obspy.UTCDateTime(0), sliprate=None,
                 center_frequency=0.0005):
        super(ForceSource, self).__init__(latitude, longitude, depth_in_m)
        self.f_r = f_r
        self.f_t = f_t
        self.f_p = f_p
        self.origin_time = origin_time
        self.sliprate = sliprate
        self.center_frequency = float(center_frequency)

    @property
    def force_tpr(self):
        """
        List of force components in theta, phi, r coordinates:
        [f_t, f_p, f_r]
        """
        return np.array([self.f_t, self.f_p, self.f_r])

    @property
    def force_rtp(self):
        """
        List of force components in r, theta, phi, coordinates:
        [f_r, f_t, f_p]
        """
        return np.array([self.f_r, self.f_t, self.f_p])

    def __str__(self):
        return_str = 'Salvus Force Source:\n'
        return_str += '\tOrigin Time      : %s\n' % (self.origin_time,)
        return_str += '\tLongitude : %6.1f deg\n' % (self.longitude)
        return_str += '\tLatitude  : %6.1f deg\n' % (self.latitude)
        return_str += '\tFr        : %10.2e N\n' % (self.f_r)
        return_str += '\tFt        : %10.2e N\n' % (self.f_t)
        return_str += '\tFp        : %10.2e N\n' % (self.f_p)

        return return_str


class Receiver(SourceOrReceiver):
    """
    Class dealing with seismic receivers.

    :type latitude: float
    :param latitude: The geocentric latitude of the receiver in degree.
    :type longitude: float
    :param longitude: The longitude of the receiver in degree.
    :type depth_in_m: float
    :param depth_in_m: The depth of the receiver in meters. Only
    :type network: str, optional
    :param network: The network id of the receiver.
    :type station: str, optional
    :param station: The station id of the receiver.
    :type location: str
    :param location: The location code of the receiver.

    >>> from salvus_source import Receiver
    >>> rec = Receiver(latitude=12.34, longitude=56.78, network="AB",
    ...                station="CDE", location="SY")
    >>> print(rec)  # doctest: +NORMALIZE_WHITESPACE
    Salvus Receiver:
        Longitude :   56.8 deg
        Latitude  :   12.3 deg
        Network   : AB
        Station   : CDE
        Location  : SY
    """
    def __init__(self, latitude=0.0, longitude=0.0, network="XX",
                 station="SYN", location="", depth_in_m=0.0):
        super(Receiver, self).__init__(latitude, longitude,
                                       depth_in_m=depth_in_m)
        self.network = network
        self.network = self.network.strip()
        assert len(self.network) <= 2

        self.station = station
        self.station = self.station.strip()
        assert len(self.station) <= 5

        self.location = location
        self.location = self.location.strip()
        assert len(self.location) <= 2

    def __str__(self):
        return_str = 'Salvus Receiver:\n'
        return_str += '\tLongitude : %6.1f deg\n' % (self.longitude)
        return_str += '\tLatitude  : %6.1f deg\n' % (self.latitude)
        return_str += '\tNetwork   : %s\n' % (self.network)
        return_str += '\tStation   : %s\n' % (self.station)
        return_str += '\tLocation  : %s\n' % (self.location)

        return return_str

    @staticmethod
    @_purge_duplicate_stations
    def parse(filename_or_obj, network_code=None):
        """
        Attempts to parse anything to a list of
        :class:`~salvus_source.source.Receiver` objects. Always
        returns a list, even if it only contains a single element. It is
        meant as a single entry point for receiver information from any source.

        Supports StationXML, the custom STATIONS fileformat, SAC files,
        SEED files, and a number of ObsPy objects. This method can
        furthermore work with anything ObsPy can deal with (filename, URL,
        memory files, ...).

        Coordinates are assumed to be defined on the WGS84 ellipsoid and
        will be converted to geocentric coordinates.

        :param filename_or_obj: Filename/URL/Python object
        :param network_code: Network code needed to parse ObsPy station
            objects. Likely only needed for the recursive part of this method.
        :return: List of :class:`~salvus_source.source.Receiver` objects.

        The following example parses a StationXML file to a list of
        :class:`~salvus_source.source.Receiver` objects.

        >>> import salvus_source
        >>> print(salvus_source.Receiver.parse(stationxml_file))
        [<salvus_source.source.Receiver object at 0x...>]
        """
        receivers = []

        # STATIONS file.
        if isinstance(filename_or_obj, (str, bytes)) and \
                os.path.exists(filename_or_obj):
            try:
                return Receiver._parse_stations_file(filename_or_obj)
            except Exception:
                pass
        # ObsPy inventory.
        elif isinstance(filename_or_obj, obspy.core.inventory.Inventory):
            for network in filename_or_obj:
                receivers.extend(Receiver.parse(network))
            return receivers
        # ObsPy network.
        elif isinstance(filename_or_obj, obspy.core.inventory.Network):
            for station in filename_or_obj:
                receivers.extend(Receiver.parse(
                    station, network_code=filename_or_obj.code))
            return receivers
        # ObsPy station.
        elif isinstance(filename_or_obj, obspy.core.inventory.Station):
            # If there are no channels, use the station coordinates.
            if not filename_or_obj.channels:
                return [Receiver(
                    latitude=elliptic_to_geocentric_latitude(
                        filename_or_obj.latitude),
                    longitude=filename_or_obj.longitude,
                    network=network_code, station=filename_or_obj.code)]
            # Otherwise use the channel information. Raise an error if the
            # coordinates are not identical for each channel. Only parse
            # latitude and longitude, as the DB currently cannot deal with
            # varying receiver heights.
            else:
                coords = set((_i.latitude, _i.longitude, _i.depth) for _i in
                             filename_or_obj.channels)
                if len(coords) != 1:
                    raise ReceiverParseError(
                        "The coordinates of the channels of station '%s.%s' "
                        "are not identical." % (network_code,
                                                filename_or_obj.code))
                coords = coords.pop()
                return [Receiver(
                    latitude=elliptic_to_geocentric_latitude(coords[0]),
                    longitude=coords[1],
                    depth_in_m=coords[2],
                    network=network_code,
                    station=filename_or_obj.code)]
        # ObsPy Stream (SAC files contain coordinates).
        elif isinstance(filename_or_obj, obspy.Stream):
            for tr in filename_or_obj:
                receivers.extend(Receiver.parse(tr))
            return receivers
        elif isinstance(filename_or_obj, obspy.Trace):
            if not hasattr(filename_or_obj.stats, "sac"):
                raise ReceiverParseError("ObsPy Trace must have an sac "
                                         "attribute.")
            if "stla" not in filename_or_obj.stats.sac or \
                    "stlo" not in filename_or_obj.stats.sac or \
                    "stdp" not in filename_or_obj.stats.sac:
                raise ReceiverParseError(
                    "SAC file does not contain coordinates for channel '%s'" %
                    filename_or_obj.id)
            coords = (filename_or_obj.stats.sac.stla,
                      filename_or_obj.stats.sac.stlo,
                      filename_or_obj.stats.sac.stdp)
            return [Receiver(
                latitude=elliptic_to_geocentric_latitude(coords[0]),
                longitude=coords[1],
                depth_in_m=coords[2],
                network=filename_or_obj.stats.network,
                station=filename_or_obj.stats.station)]

        # Check if its anything ObsPy can read and recurse.
        try:
            return Receiver.parse(obspy.read_inventory(filename_or_obj))
        except ReceiverParseError as e:
            raise e
        except Exception:
            pass

        # SAC files contain station coordinates.
        try:
            return Receiver.parse(obspy.read(filename_or_obj))
        except ReceiverParseError as e:
            raise e
        except Exception:
            pass

        raise ValueError("%s could not be parsed." % repr(filename_or_obj))

    @staticmethod
    def _parse_stations_file(filename):
        """
        Parses a custom STATIONS file format to a list of Receiver objects.

        Coordinates are assumed to be defined on the WGS84 ellipsoid and
        will be converted to geocentric coordinates.

        :param filename: Filename
        :return: List of :class:`~salvus_source.source.Receiver` objects.
        """
        with open(filename, 'rt') as f:
            receivers = []

            for line in f:
                station, network, lat, lon, _, _ = line.split()
                lat = elliptic_to_geocentric_latitude(float(lat))
                lon = float(lon)
                receivers.append(Receiver(lat, lon, network, station))

        return receivers
