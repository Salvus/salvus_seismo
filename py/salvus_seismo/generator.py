import collections
import copy
import csv
import io
import os
import sys
import warnings

import numpy as np

from .source import Source, ForceSource
from .helpers import get_transformation_matrix_3d, \
    get_transformation_matrix_2d, string_to_list


class Config(object):
    def __init__(self, mesh_file, end_time,
                 dimensions,
                 salvus_call="salvus",
                 start_time=None,
                 time_step=None,
                 receiver_file_name="receiver.h5",
                 receiver_fields=("u_ELASTIC",),
                 movie_file_name=None,
                 movie_fields=[],
                 wavefield_file_name=None,
                 wavefield_fields=[],
                 polynomial_order=4,
                 absorbing_boundaries=None,
                 num_absorbing_layers=None,
                 with_anisotropy=False,
                 verbose=True):
        self.mesh_file = str(mesh_file)
        self.start_time = float(start_time) if start_time is not None else None
        self.time_step = float(time_step) if time_step is not None else None
        self.end_time = float(end_time)
        self.polynomial_order = int(polynomial_order)
        self.verbose = verbose
        self.dimensions = int(dimensions)
        self.salvus_call = salvus_call
        self.absorbing_boundaries = absorbing_boundaries
        self.with_anistropy = with_anisotropy

        self.num_absorbing_layers = float(num_absorbing_layers) if \
            num_absorbing_layers is not None else None

        self.movie_file_name = movie_file_name
        if self.movie_file_name:
            assert movie_fields
            self.movie_fields = movie_fields

        self.wavefield_file_name = wavefield_file_name
        if self.wavefield_file_name:
            assert wavefield_fields
            self.wavefield_fields = wavefield_fields

        self.receiver_file_name = receiver_file_name
        self.receiver_fields = receiver_fields

        if self.dimensions not in (2, 3):
            raise ValueError("Only 2D or 3D allowed.")


def generate_cli_call(source, receivers, config, output_folder,
                      exodus_file=None):
    """
    Generate the command line string and input files to run salvus.

    :param source: The sources object.
    :param receivers: One or more receivers.
    :param config: The advanced configuration.
    :param output_folder: The output folder. Must not exist yet.
    :param exodus_file: Filename of the exodus mesh. If given, receivers
        will be placed exactly at the mesh surface, even with topography.
        This is separate from the mesh_file in the config object as
        salvus_seismo will often be run on a different machine then salvus
        itself.
    """
    if os.path.exists(output_folder):
        raise ValueError("The output folder must not yet exist.")
    os.makedirs(output_folder)

    if config.dimensions not in (2, 3):
        raise ValueError("Only 2D or 3D.")
    dimensions = config.dimensions

    source = copy.deepcopy(source)
    receivers = copy.deepcopy(receivers)

    absorbing_boundaries = config.absorbing_boundaries

    # If an exodus file is given, use it for more accurate receiver placement.
    if exodus_file:
        from . import locate_receiver
        import pyexodus

        with pyexodus.exodus(exodus_file, mode="r") as e:
            ex_dims = e.num_dims
            side_set_names = e.get_side_set_names()
        if ex_dims != config.dimensions:
            # Exodus file dimensions overwrite manual dimension setting.
            msg = ("Dimensions in exodus file (%i) are not equal to the "
                   "configured dimensions (%i). Will use the exodus "
                   "dimensions." % (ex_dims, config.dimensions))
            warnings.warn(msg)
            dimensions = ex_dims

        if dimensions == 2:
            r_loc = locate_receiver.ReceiverLocator2D(exodus_file=exodus_file)
        elif dimensions == 3:
            r_loc = locate_receiver.ReceiverLocator(exodus_file=exodus_file)
        else:
            raise NotImplementedError

        if absorbing_boundaries is None:
            possible_boundaries = set(("r0", "t0", "t1", "p0", "p1",
                                       "inner_boundary"))
            absorbing_boundaries = \
                possible_boundaries.intersection(set(side_set_names))
            if absorbing_boundaries:
                absorbing_boundaries = sorted(absorbing_boundaries)
                print("Automatically determined the following absorbing "
                      "boundary side sets: %s" % ", ".join(
                        absorbing_boundaries))

    else:
        r_loc = None

    if dimensions == 2:
        # Latitude will be zero for all 2D things.
        source.latitude = 0
        for receiver in receivers:
            receiver.latitude = 0
    elif dimensions == 3:
        pass
    else:
        raise NotImplementedError

    args = collections.OrderedDict()
    src_args = collections.OrderedDict()

    args["dimension"] = str(dimensions)
    args["mesh-file"] = config.mesh_file
    args["model-file"] = config.mesh_file

    if config.start_time is not None:
        args["start-time"] = str(config.start_time)

    if config.time_step is not None:
        args["time-step"] = str(config.time_step)

    if config.num_absorbing_layers is not None:
        args["num-absorbing-layers"] = config.num_absorbing_layers

    args["end-time"] = str(config.end_time)
    args["polynomial-order"] = config.polynomial_order

    args["receiver-file-name"] = config.receiver_file_name
    args["receiver-fields"] = ",".join(config.receiver_fields)

    if config.with_anistropy:
        args["with-anisotropy"] = None

    # Source
    src_args["source_temporal_type"] = source.sliprate

    if source.sliprate == "ricker":
        src_args["source_center_frequency"] = source.center_frequency

    if absorbing_boundaries:
        args["absorbing-boundaries"] = ",".join(absorbing_boundaries)

    if config.movie_file_name:
        args["save-movie"] = None
        args["movie-file-name"] = config.movie_file_name
        args["movie-fields"] = ",".join(config.movie_fields)

    if config.wavefield_file_name:
        args["save-wavefield-file"] = config.wavefield_file_name
        args["save-fields"] = ",".join(string_to_list(config.wavefield_fields))

    if config.verbose:
        args["verbose"] = None

    x_s, y_s, z_s = source.x(), source.y(), source.z()

    # Bury the sources relative to the actual mesh surface.
    if r_loc:
        p = r_loc.search(np.array([x_s, y_s, z_s]))

        # Bury sources - only needs to be done when placed on the
        # surface of the mesh - otherwise its already done.
        _s = p / np.linalg.norm(p)
        # Everything is in meters.
        p -= _s * source.depth_in_m

        if dimensions == 2:
            x_s, y_s = p
        elif dimensions == 3:
            x_s, y_s, z_s = p
        else:
            raise NotImplementedError

    if dimensions == 2:
        src_args["source_location"] = [x_s, y_s]
    elif dimensions == 3:
        src_args["source_location"] = [x_s, y_s, z_s]
    else:
        raise NotImplementedError

    _source_csv_data = [u"%g" % x_s, u"%g" % y_s, u"%g" % z_s,
                        u"Source"]

    # Tensor source.
    if isinstance(source, Source):
        src_args["source_spatial_type"] = "moment_tensor"
        if dimensions == 2:
            M = get_transformation_matrix_2d(longitude=source.longitude)
            M_inv = np.linalg.inv(M)
            mt = np.array([[source.m_rr, source.m_rp],
                           [source.m_rp, source.m_pp]])
            mt = M_inv.dot(mt.dot(M_inv.T))

            # Pass it in voigt notation. (Need to remove '+' as PETSc does
            # not like.
            src_args["source_scale"] = \
                ",".join("%#g" % _i for _i in [
                    mt[0, 0], mt[1, 1], mt[0, 1]]).replace("+", "")
        elif dimensions == 3:
            M = get_transformation_matrix_3d(latitude=source.latitude,
                                             longitude=source.longitude)
            # Matrix currently goes from x,y,z to ZNE. MT is rtp. N = -theta.
            M[1] *= -1.0
            M_inv = np.linalg.inv(M)
            mt = np.array([[source.m_rr, source.m_rt, source.m_rp],
                           [source.m_rt, source.m_tt, source.m_tp],
                           [source.m_rp, source.m_tp, source.m_pp]])

            mt = M_inv.dot(mt.dot(M_inv.T))

            # Pass it in voigt notation. (Need to remove '+' as PETSc does
            # not like).
            src_args["source_scale"] = \
                ",".join("%#g" % _i for _i in [
                    mt[0, 0], mt[1, 1], mt[2, 2],
                    mt[1, 2], mt[0, 2], mt[0, 1]]).replace("+", "")
        else:
            raise NotImplementedError
    # Vector source.
    elif isinstance(source, ForceSource):
        src_args["source_spatial_type"] = "vector"

        if dimensions == 2:
            M_inv = np.linalg.inv(get_transformation_matrix_2d(
                longitude=source.longitude))
            src_args["source_scale"] = ",".join(
                "%g" % _i for _i in np.dot(
                    M_inv, [source.f_r, source.f_p])).replace("+", "")
        elif dimensions == 3:
            # Goes from Z, N, E to xyz
            M_inv = np.linalg.inv(get_transformation_matrix_3d(
                latitude=source.latitude, longitude=source.longitude))
            # Theta is south -> invert
            src_args["source_scale"] = ",".join(
                "%#g" % _i for _i in np.dot(
                    M_inv, [source.f_r, -source.f_t,
                            source.f_p])).replace("+", "")
        else:
            raise NotImplementedError
    else:
        raise TypeError("The source must either be a moment tensor or a "
                        "force source.")

    args["receiver-toml"] = os.path.join(output_folder, "receivers.toml")
    args['source-toml'] = os.path.join(output_folder, "source.toml")

    source_string = u"[[source]]\n" \
                    u"name = \"source1\"\n"

    # Write source to a TOML file.
    for key, value in src_args.items():
        if key == "source_scale":
            source_string += "%s = [%s]\n" % (key[7:], str(value))
        elif isinstance(value, str):
            source_string += "%s = \"%s\"\n" % (key[7:], str(value))
        else:
            source_string += "%s = %s\n" % (key[7:], str(value))

    with io.open(args['source-toml'], "wt") as fh:
        fh.write(source_string)

    cmd = u"" + config.salvus_call
    for key, value in args.items():
        if value is None:
            cmd += " --%s" % key
        else:
            cmd += " --%s %s" % (key, str(value))

    with io.open(os.path.join(output_folder, "run_salvus.sh"), "wt") as fh:
        fh.write(cmd)

    _csv_data = []

    # Finally convert the receivers to a TOML file.
    receiver_counter = 0
    with io.open(os.path.join(output_folder, "receivers.toml"), "wt") as fh:
        for receiver in receivers:
            x = receiver.x()
            y = receiver.y()
            z = receiver.z()
            # Locate the receiver exactly relative to the mesh surface,
            # if an exodus mesh has been passed. Otherwise the mesh is
            # assumed to be a perfect sphere.
            if r_loc:
                try:
                    p = r_loc.search(np.array([x, y, z]))
                except Exception:
                    msg = "Could not locate station %s.%s on mesh. Outside " \
                          "of mesh? latitude = %6.2f, longitude = %6.2f" % (
                              receiver.network.strip(),
                              receiver.station.strip(),
                              receiver.longitude, receiver.latitude)
                    print(msg)
                    continue

                # Bury receivers - only needs to be done when placed on the
                # surface of the mesh - otherwise its already done.
                _s = p / np.linalg.norm(p)
                # Everything is in meters.
                p -= _s * receiver.depth_in_m

                if dimensions == 2:
                    x, y = p
                elif dimensions == 3:
                    x, y, z = p
                else:
                    raise NotImplementedError

            # Collect information here to assure consistency between TOML
            # and CSV file.
            _csv_data.append([u"%g" % x, u"%g" % y, u"%g" % z,
                              u"%s.%s" % (receiver.network.strip(),
                                          receiver.station.strip())])

            if dimensions == 2:
                salvus_coordinates = "[{x:.5f}, {y:.5f}]".format(x=x, y=y)
                M = get_transformation_matrix_2d(
                    longitude=receiver.longitude)
                transformation_matrix_str = (
                    "transform_matrix = [ [{m_xx:.5f}, {m_xy:.5f}],\n"
                    "                     [{m_yx:.5f}, {m_yy:.5f}] ]") \
                    .format(m_xx=M[0][0], m_xy=M[0][1],
                            m_yx=M[1][0], m_yy=M[1][1])
            elif dimensions == 3:
                salvus_coordinates = "[{x:.5f}, {y:.5f}, {z:.5f}]".format(
                    x=x, y=y, z=z)
                M = get_transformation_matrix_3d(
                    latitude=receiver.latitude,
                    longitude=receiver.longitude)
                transformation_matrix_str = (
                    "transform_matrix = [ [{m_xx:.5f}, {m_xy:.5f}, "
                    "{m_xz:.5f}],""\n"
                    "                     [{m_yx:.5f}, {m_yy:.5f}, "
                    "{m_yz:.5f}],""\n"
                    "                     [{m_zx:.5f}, {m_zy:.5f}, "
                    "{m_zz:.5f}] ]") \
                    .format(m_xx=M[0][0], m_xy=M[0][1], m_xz=M[0][2],
                            m_yx=M[1][0], m_yy=M[1][1], m_yz=M[1][2],
                            m_zx=M[2][0], m_zy=M[2][1], m_zz=M[2][2])
            else:
                raise NotImplementedError

            fh.write(
                u"[[receiver]]\n"
                "network = \"{network}\"\n"
                "station = \"{station}\"\n"
                "location = \"{location}\"\n"
                "physical_latitude = {latitude:.5f}\n"
                "physical_longitude = {longitude:.5f}\n"
                "physical_depth_in_meters = {depth_in_meters:.5f}\n"
                # XXX: This is currently hard-coded. Must be read from the
                # mesh at one point!
                "medium = \"solid\"\n"
                "salvus_coordinates = {salvus_coordinates}\n"
                "{transformation_matrix_str}\n\n".format(
                    network=receiver.network.strip(),
                    station=receiver.station.strip(),
                    location=receiver.location.strip(),
                    latitude=receiver.latitude,
                    longitude=receiver.longitude,
                    depth_in_meters=receiver.depth_in_m,
                    salvus_coordinates=salvus_coordinates,
                    transformation_matrix_str=transformation_matrix_str)
            )

            receiver_counter += 1

    print("Wrote %i receivers into the TOML file." % receiver_counter)

    # Also write a CSV file for paraview. Different behavior with py 2 / 3.
    rec_file = os.path.join(output_folder, "receivers_paraview.csv")
    src_file = os.path.join(output_folder, "source_paraview.csv")
    if sys.version_info[0] < 3:
        rec_fh = io.open(rec_file, "wb")
        src_fh = io.open(src_file, "wb")
    else:
        rec_fh = io.open(rec_file, "wt", newline='')
        src_fh = io.open(src_file, "wt", newline='')

    with rec_fh as fh:
        writer = csv.writer(fh, delimiter=",", quoting=csv.QUOTE_MINIMAL)
        writer.writerow([u"x coord", u"y coord", u"z_coord", u"station"])
        for _i in _csv_data:
            writer.writerow(_i)

    # Same for the source.
    with src_fh as fh:
        writer = csv.writer(fh, delimiter=",", quoting=csv.QUOTE_MINIMAL)
        writer.writerow([u"x coord", u"y coord", u"z_coord", u"station"])
        writer.writerow(_source_csv_data)
