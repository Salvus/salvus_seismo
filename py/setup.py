#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Seismological Python interface for Salvus.

:copyright:
    Salvus Development Team (salvus@googlegroups.com), 2016
:license:
    Mozilla Public License Version 2.0
    (https://www.mozilla.org/en-US/MPL/2.0/)
"""
import inspect
import os
import sys

from setuptools import setup, find_packages


# Import the version string.
path = os.path.join(os.path.abspath(os.path.dirname(inspect.getfile(
    inspect.currentframe()))), "salvus_seismo")
sys.path.insert(0, path)
from version import get_git_version  # NOQA


def get_package_data():
    """
    Returns a list of all files needed for the installation relative to the
    'salvus_seismo' subfolder.
    """
    filenames = []
    # The lasif root dir.
    root_dir = os.path.join(os.path.dirname(os.path.abspath(
        inspect.getfile(inspect.currentframe()))), "salvus_seismo")
    # Recursively include all files in these folders:
    folders = [os.path.join(root_dir, "tests", "data")]
    for folder in folders:
        for directory, _, files in os.walk(folder):
            for filename in files:
                # Exclude hidden files.
                if filename.startswith("."):
                    continue
                filenames.append(os.path.relpath(
                    os.path.join(directory, filename),
                    root_dir))
    return filenames


setup_config = dict(
    name="salvus_seismo",
    version=get_git_version(),
    description="Seismological Python interface for Salvus",
    author="Salvus Development Team",
    author_email="salvus@googlegroups.com",
    url="https://gitlab.com/Salvus/salvus.seismo",
    packages=find_packages(),
    license="MPLv2",
    platforms="OS Independent",
    install_requires=["numpy"],
    package_data={
        "salvus_seismo": get_package_data()},
)


if __name__ == "__main__":
    setup(**setup_config)
